package unipg.iO

import org.apache.spark.rdd.RDD
import org.apache.spark.graphx.GraphLoader
import org.apache.spark.graphx.Graph
import org.apache.spark.graphx.VertexId
import org.apache.spark.SparkContext
import unipg.utils.VP
import scala.collection.mutable.ListBuffer

/**
 * This object contains the method for importing the graph
 */

object InputGraph extends java.io.Serializable {  
  
  /**
 	* This method builds a graph starting from a text file (Edge List)
 	* 
 	* @param SparkContext
 	* @param input-path
 	* @param Time To Live
 	* @return The list of the connected components
 	*/
  
  def getVerticesAndEdgesFromEdgesByPackageAndSC(sc : SparkContext, path : String, ttl : Int) : 
  ListBuffer[(Graph[(Int, VP),Boolean])] = {
    
    var partial = GraphLoader.edgeListFile(sc, path).connectedComponents();       
    val vertices : RDD[(VertexId, (Int, VP))] = partial.vertices.map{
      vertex =>
            val v = new VP;
            val cc = vertex._2
            
            v.id = vertex._1;
            v.cc = cc
            (vertex._1, (ttl, v));
        }
    
    val edges = partial.edges.mapValues{
      edge =>
        true;
    }
    
    val graph = Graph(vertices, edges).cache()
    
    val lista = partial.vertices.map{ case(_,cc) => cc}.distinct().collect().toList
    val listCC = new ListBuffer[Graph[(Int, VP),Boolean]]()
    
    for(index <- lista){
      val frazGraph = graph.subgraph(vpred = (ttl, vp) => vp._2.cc == index)
      listCC += frazGraph
    }
   
    listCC.sortWith(_.vertices.count() > _.vertices.count())
   }
      
}