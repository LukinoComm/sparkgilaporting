package unipg.iO

import java.io.PrintWriter
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.spark.graphx.Graph
import org.apache.spark.graphx.Edge
import unipg.utils.VP

/**
 * This object contains the method for exporting the graph
 */

object ToGML extends Serializable {
  
  /**
 	* This method transforms the graph into a gml file
 	* @param <output-path>
 	* @param The Graph
 	*/
  
  def convertToGML(path : String, input : Graph[(Int, VP),Boolean])= {
    
    val conf = new Configuration()
    val fs = FileSystem.get(conf)
    val output = fs.create(new Path(path))
    val writer = new PrintWriter(output)
    
    var string : String = "graph [\n\tdirected 0"
    val iteratorVertices = input.vertices.toLocalIterator
    
    while(iteratorVertices.hasNext){    
      val vertex = iteratorVertices.next()
      
      string = string+"\n\tnode [\n\t\t"+"id "+vertex._1+"\n\t\tgraphics [\n\t\t\tx "+vertex._2._2.coord_x+
      "\n\t\t\ty "+vertex._2._2.coord_y+
      "\n\t\t\tw 3\n\t\t\th 3\n\t\t]\n\t]\n"
    }
       
    val iteratorEdges = input.edges.map(
      edge => 
        Edge(Math.min(edge.dstId,edge.srcId),Math.max(edge.dstId,edge.srcId),true)    
    ).distinct().toLocalIterator
    
    while(iteratorEdges.hasNext){
      val edge = iteratorEdges.next()
      
      string = string+"\n\tedge [\n\t\tsource "+edge.srcId+"\n\t\t"+"target "+edge.dstId+
      "\n\t\tgraphics [\n\t\t\ttype \"line\"\n\t\t]\n\t]\n"
    }
 
    string = string + "\n]"
    writer.write(string)
    writer.close()   
  }
  
}