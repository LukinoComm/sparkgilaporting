package unipg.operations

import scala.collection.mutable.ListBuffer
import org.apache.spark.graphx.VertexId
import org.apache.spark.graphx.Edge
import unipg.utils.VP

/**
 * This object constructs "a graph", made up of the vertices to be reinserted
 */

object GraphFaber extends java.io.Serializable {
  
  var vertices_list : ListBuffer[(VertexId, (Int, VP))] = new ListBuffer() 
  var edges_list : ListBuffer[Edge[Boolean]] = new ListBuffer()
  
  /**
   * This method creates the vertices to be reinserted and the relative edges
   * @param ID Vertex to insert
   * @param Vertex Coordinates
   * @param Vertex Neighbor
   */
  
  def addVertexAndEdges(idOfOneEdge : Long, coordinatesOfVertexToPlace : Array[Double], neighborVertex : VP) = {
    
    var new_vertex = (idOfOneEdge, (0, new VP))
    new_vertex._2._2.coord_x = coordinatesOfVertexToPlace(0)
    new_vertex._2._2.coord_y = coordinatesOfVertexToPlace(1)
    new_vertex._2._2.cc = neighborVertex.cc
    
    val new_edge_1 = Edge(idOfOneEdge, neighborVertex.id, true)
    val new_edge_2 = Edge(neighborVertex.id, idOfOneEdge, true)
    
    vertices_list = (vertices_list += new_vertex)
    edges_list = (edges_list += new_edge_1)
    edges_list = (edges_list += new_edge_2)
    
  }
    
  /**
   * @return vertices's list
   */
  
  def getVerticesList() : ListBuffer[(VertexId, (Int, VP))] = {
    val result = vertices_list.clone()
    vertices_list.clear()
    result  
  }
  
   /**
   * @return edges' list
   */
  def getEdgesList() : ListBuffer[Edge[Boolean]] = {
    val result = edges_list.clone()
    edges_list.clear()
    result  
  }
  
}