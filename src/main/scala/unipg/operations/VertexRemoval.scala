package unipg.operations

import org.apache.spark.graphx.VertexId
import org.apache.spark.graphx.Graph
import org.apache.spark.graphx.EdgeDirection
import scala.collection.mutable.ListBuffer
import org.apache.spark.rdd.RDD
import org.apache.spark.graphx.Graph.graphToGraphOps
import org.apache.spark.rdd.RDD.rddToPairRDDFunctions
import unipg.utils.VP

/**
 * This object manages the one-degree-vertices' removal
 */

object VertexRemoval extends java.io.Serializable {
  
  /**
   * This method removes these vertices and adds them to the neighbour's adjacent_list_one_degree
   * @param The Graph
   * @return A new graph, without these vertices and edges
   */
  
  def removeOneDegreeVertices(graph : Graph[(Int, VP),Boolean]) : (Graph[(Int, VP),Boolean]) = {
    
    val collectN = graph.collectNeighbors(EdgeDirection.Out)
    val oneDegVertList = collectN.filter{
      element =>
        element._2.length == 1}           
        .map{values => values._1 }
        .collect().toList          
      
    val mapCorrespondances = collectN.map{
      v=>
        
        val list = ListBuffer[Long]() 
        
        if(v._2.length == 1){
          // do-nothing  
        }
        
        else if (v._2.length >= 1){
          for(neigh <- v._2){ 
            if(oneDegVertList.contains(neigh._1)){
              list += neigh._1
            }
          }
        }
        
        (v._1, list)
      }.collectAsMap()
      
  
    val v_upd : RDD[(VertexId, (Int, VP))] = graph.vertices.map{
      vertex =>
        val list = mapCorrespondances.get(vertex._1).get
        vertex._2._2.adjacent_list_one_degree = vertex._2._2.adjacent_list_one_degree ++ list
        (vertex._1, (vertex._2._1, vertex._2._2))
      }
      
      val g = Graph(v_upd, graph.edges)
      val newGraph =  g.subgraph(epred = (x) => (!oneDegVertList.contains(x.srcId) && (!oneDegVertList.contains(x.dstId))), 
        vpred = (id, attr) => (!oneDegVertList.contains(id)))
        
      g.unpersist()
      newGraph
    
  }
  
}