package unipg.operations

import unipg.forces.FruchtermanReingold
import org.apache.spark.graphx.VertexId
import scala.Iterator
import unipg.utils.Counter
import unipg.utils.VP

 /**
 * This class contains the pregel-methods for aggregation information about the neighborhood of each vertex
 */

class PregelPorting (k : Int, t_x : Double, t_y : Double) extends java.io.Serializable {
  
  import scala.collection.mutable.ListBuffer
  import org.apache.spark.graphx.EdgeTriplet
  import org.apache.spark.graphx.VertexId
  import scala.collection.mutable.Map
  
  val map_init : Map[VertexId,(Double, Double)] = Map();
  val initialMsg = (map_init,k+1);
  
  var TX = t_x
  var TY = t_y
  
  /**
   * These methods set the previous variables
   */
  
  def setTX(newValue : Double) = {
    TX = newValue
  }
  
  def setTY(newValue : Double) = {
    TY = newValue
  }
  
 /**
 	* In this method the vertex sends its coordinates to the neighbors 
 	* @param An EdgeTriplet that describes the vertex
 	* @return An Iterator, according to pregel schema
 	*/
  
  def sendMsg(triplet: EdgeTriplet[(Int, VP), Boolean]): Iterator[(VertexId, (Map[VertexId,(Double, Double)],Int))] = {

    val sourceVertex = triplet.srcAttr   
    val ttl_upd = sourceVertex._1-1   
    if(ttl_upd>0){
      val itself = Map(triplet.srcId -> (triplet.srcAttr._2.coord_x, triplet.srcAttr._2.coord_y))
      val snd_update_map = sourceVertex._2.map ++ itself 
      Iterator((triplet.dstId,(snd_update_map, ttl_upd)));
    }
    else{
      Iterator.empty
    }
    
  }
  
 /**
 	* This method merges two messages into a single message
 	* @param Two maps to merge
 	* @return The output message
 	*/
  
  def mergeMsg(msg1: (Map[VertexId,(Double, Double)],Int), msg2: (Map[VertexId,(Double, Double)],Int))
  : (Map[VertexId,(Double, Double)],Int) = {
    
    var map_res : Map[VertexId,(Double, Double)] = Map();
    map_res = map_res ++ msg1._1
    map_res = map_res ++ msg2._1 

    return (map_res,msg1._2)
  }
  
  /**
	* In this method the vertex calculates the repulsive and attrative 
  * forces and defines the message to be sent to the next superstep
 	* @param VertexId
 	* @param The message
 	* @return The Vertex
 	*/
  
  def vprog(vertexId: VertexId, value: (Int, VP), 
      message: (Map[VertexId,(Double, Double)],Int)): 
      ((Int, VP)) = {

    if(message == initialMsg){
      (initialMsg._2,value._2)
    }
    
    else{
      var coord_upd_x  = value._2.x_force
      var coord_upd_y  = value._2.y_force     
      var map_to_send : Map[VertexId,(Double, Double)] = Map();
      
      message._1.keys.foreach(    
           i => 
             if(!value._2.list_VertexID.contains(i) && i!=vertexId){            
               // Attraction
               if(message._2 == (k)){
                 val attraction = FruchtermanReingold.computeAttraction(message._1(i), (value._2.coord_x, value._2.coord_y))
                 coord_upd_x = coord_upd_x + attraction._1;
                 coord_upd_y = coord_upd_y + attraction._2;
               }              
               // Repulsion
               val repulsion = FruchtermanReingold.computeRepulsion(message._1(i), (value._2.coord_x, value._2.coord_y))
               coord_upd_x = coord_upd_x - repulsion._1;
               coord_upd_y = coord_upd_y - repulsion._2;          
               value._2.list_VertexID+=(i)
               val add : Map[VertexId,(Double, Double)] = Map(i.toLong -> (message._1(i)._1,message._1(i)._2))
               map_to_send = map_to_send ++ add
             }      
       )
  
         
      var a : Double = 0.0
      var b : Double = 0.0     
      if(message._2 == 1){    
        a = (coord_upd_x/Math.abs(coord_upd_x)) * Math.min(Math.abs(coord_upd_x), TX)
        b = (coord_upd_y/Math.abs(coord_upd_y)) * Math.min(Math.abs(coord_upd_y), TY)      
        if(a.abs<=0.2 && b.abs<=0.2){  
          Counter.c = Counter.c + 1
        } 
      }
           
      value._2.map = map_to_send
      value._2.coord_x = value._2.coord_x+a
      value._2.coord_y = value._2.coord_y+b
      value._2.x_force = coord_upd_x
      value._2.y_force = coord_upd_y
      
     ((message._2), value._2)
    }   
   }
  

  
}