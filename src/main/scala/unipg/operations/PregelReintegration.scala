package unipg.operations

import org.apache.spark.graphx.VertexId
import scala.Iterator
import unipg.utils.VP

 /**
 * This class contains the pregel-methods for the reintegration of the vertices
 */

class PregelReintegration extends java.io.Serializable {
  
  import scala.collection.mutable.ListBuffer
  import org.apache.spark.graphx.EdgeTriplet
  import org.apache.spark.graphx.VertexId
  import scala.collection.mutable.Map
  
  val map_init : Map[VertexId,(Double, Double)] = Map();
  val initialMsg = (map_init);
  
  /**
 	* In this method the vertex sends its coordinates to the neighbors 
 	* @param An EdgeTriplet that describes the vertex
 	* @return An Iterator, according to pregel schema
 	*/
  
  def sendMsg(triplet: EdgeTriplet[(Int, VP), Boolean]): Iterator[(VertexId, (Map[VertexId,(Double, Double)]))] = {   
    Iterator((triplet.dstId,(Map(triplet.srcId -> (triplet.srcAttr._2.coord_x, triplet.srcAttr._2.coord_y)))));
  }
  
  /**
 	* This method merges two messages into a single message
 	* @param Two maps to merge
 	* @return The output message
 	*/
  
  def mergeMsg(msg1: (Map[VertexId,(Double, Double)]), msg2: (Map[VertexId,(Double, Double)]))
  : (Map[VertexId,(Double, Double)]) = {
    
    var map_res : Map[VertexId,(Double, Double)] = Map();
    map_res = map_res ++ msg1
    map_res = map_res ++ msg2
    
    return (map_res)
  }
  
  /**
 	* In this method the vertex calculates the length of the longest edge
 	* and deals with the insertion of the one-degree-vertices, if present
 	* 
 	* @param VertexId
 	* @param The message
 	* @return The Vertex
 	* 
 	*/
  
  def vprog(vertexId: VertexId, value: (Int, VP), 
      message: (Map[VertexId,(Double, Double)])): 
      ((Int, VP)) = {
        
    if(message == initialMsg){
      (value._1,value._2)
    }
    
    else{      
      var minEdgeLength = Double.MaxValue   
      value._2.map = message
      var i = 0
      for ((k,v) <- message){
        val delta_x = value._2.coord_x - v._1
        val delta_y = value._2.coord_y - v._2
        var distance = Math.sqrt(Math.pow(delta_x, 2) + Math.pow(delta_y, 2))
        if(distance < minEdgeLength)
          minEdgeLength = distance    
      }
           
      if(value._2.adjacent_list_one_degree.size > 0){ 
        val rehabilitationOneDegreeVertices = new ReintegrationOneDegreeVertices(value._2,
          minEdgeLength.toInt, value._2.map.size, value._2.adjacent_list_one_degree.length)   
        rehabilitationOneDegreeVertices.computePositions()
      } 
      (value._1, value._2)
    }
    
   }
  
}