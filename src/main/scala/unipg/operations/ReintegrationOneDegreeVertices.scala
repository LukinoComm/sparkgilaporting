package unipg.operations

import unipg.utils.VP
import unipg.utils.Shared

/**
 * This class reintegrates all the one degree vertices of a vertex among the slopes 
 * formed by its neighbors. It inspired by FairShareReintegrateOneEdges
 * (GiLA)
 */

class ReintegrationOneDegreeVertices (vertex: VP, shortestEdge: Int, sizeNeighborhood : Int, oneDegreeVertices : Int) extends java.io.Serializable {
  
  import java.math
  import scala.collection.mutable.ListBuffer
  import util.control.Breaks._
  import java.util.NoSuchElementException
  import java.io.IOException
  import java.util.HashMap
  
  val k = Shared.ns + (Math.sqrt(Math.pow(Shared.nl, 2) + Math.pow(Shared.nw, 2)))
  val radius = Shared.radius
  val padding = Shared.padding
  
  val size = oneDegreeVertices
  val lowThreshold = 2.0 * Math.PI / 180
  val graphR = new GraphReintegration(k, radius) 
  var maxSlope = Double.MinValue
  var selectedStart = 0.0  
  val slopes = new HashMap[Double, Double]()
  
  
  def computePositions() : Unit = {
    
    if(sizeNeighborhood == 0){
      val verticesCollection = graphR.computeOneDegreeVerticesCoordinates(vertex, shortestEdge, oneDegreeVertices, 
         (Math.PI*2).toFloat, 0)
      graphR.reconstructGraph(verticesCollection, vertex)
      // return     
    }   
    
    if (sizeNeighborhood == 1){
   
      val coordinates = (vertex.map.head._2._1, vertex.map.head._2._2)
      val computedAtan = Math.atan2(coordinates._2 - vertex.coord_y, coordinates._1 - vertex.coord_x);
			graphR.reconstructGraph(graphR.computeOneDegreeVerticesCoordinates(vertex, shortestEdge, 
			    oneDegreeVertices, (Math.PI*2).toFloat, 
						computedAtan), vertex);
			// return  
    }
    
    if (sizeNeighborhood > 1){      
      var firstTime : Boolean = true;
  		var lastSlope : Double = 0.0;
  		
  		val tempSlopesList = buildSlopesList(vertex).sortWith(_ > _); 
  		val it = tempSlopesList.iterator
  		
  		var counter = 0;
  		var pie = 0.0;
  		var catchB = false
	
      while(it.hasNext || counter < tempSlopesList.length){
        var currentSlope = 0.0;	
  				try{
  				  
  					currentSlope = it.next();				
  				
  				}
  				catch{			  
  				  case nsee : NoSuchElementException => {
  				      storeInformation((Math.PI*2 - pie).toDouble, (lastSlope).toDouble)
  					    counter = counter + 1	
  					    catchB = true	    				
  				  }
  				} 

  				if(firstTime && !catchB){
  					  firstTime = false;
  					  lastSlope = currentSlope;
  				  }
  				
          else if((!firstTime && !catchB)){
  				  val slope = (Math.abs(currentSlope - lastSlope)).toDouble
				    pie = pie + slope;
				    storeInformation(slope, lastSlope.toDouble);
				    lastSlope = currentSlope;
				    counter = counter + 1;	
          }
  			}
  			rebuild(size, vertex);
    }
	}
  
  private def buildSlopesList(vertex : VP) : ListBuffer[Double] = {

			val tempList = new ListBuffer[Double]();
  	  val myCoordinates = (vertex.coord_x, vertex.coord_y)
  	  
			for ((k,v) <- vertex.map){	
				val coordinates = (v._1, v._2)
				//vertex.getValue().setShortestEdge(Toolbox.computeModule(myCoordinates, coordinates));
        val computedAtan = Math.atan2(coordinates._2 - myCoordinates._2, coordinates._1 - myCoordinates._1);
				tempList+=(computedAtan);
			}
			return tempList;

		}
  
  private def storeInformation(slope : Double, startSlope : Double){

    if(slope >= lowThreshold + padding*2){  
				var modifiedSlope = slope;
				while(slopes.containsKey(modifiedSlope)){
					modifiedSlope = (slope + Math.random()/2).toDouble
				}
				slopes.put(modifiedSlope, startSlope + padding);
			}
		}

  private def rebuild (size : Int, vertex : VP) = {
    
    if(slopes.size() == 0){
				val verticesToPlace = graphR.computeOneDegreeVerticesCoordinates(vertex, shortestEdge, size, 0, 0);
				graphR.reconstructGraph(verticesToPlace, vertex);		
			}
    
    else{
      
			val it = slopes.entrySet().iterator();
			val oneDegreesIterator = vertex.adjacent_list_one_degree.toIterator			
			var remaining = size;
			
			while(it.hasNext() && remaining > 0){
			  
				val current = it.next();
				var quantity = 0;

				if(!it.hasNext()){
					quantity = remaining;
				}
				else{
					quantity = (Math.round(remaining * ((current.getKey()/(Math.PI*2)) % 1.0))).intValue()
				}			
				
				val interm = graphR.computeOneDegreeVerticesCoordinates(vertex, shortestEdge, quantity, current.getKey, current.getValue)
				graphR.reconstructGraphKeepingIterator(interm, vertex, oneDegreesIterator);	
				remaining -= quantity;
			}

			if(oneDegreesIterator.hasNext)
				throw new IOException("OneEdges iterator was not fully explored.");
		}
    
    
  }
  
  
  
  
  
}