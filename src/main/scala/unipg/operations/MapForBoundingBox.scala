package unipg.operations

import org.apache.spark.graphx.Graph
import scala.collection.mutable.Map
import java.awt.geom.Point2D
import unipg.utils.Shared
import unipg.utils.VP


object MapForBoundingBox extends java.io.Serializable {
  
  /**
  *  This method computes the Bounding Box for the input connected component
  *  @param the whole connected component
  */

  def mergeValue (graph : Graph[(Int, VP),Boolean])  = {
    
    var maxX = Double.MinValue
    var maxY = Double.MinValue
    
    var minX = Double.MaxValue
    var minY = Double.MaxValue
    
    val vertices = graph.vertices.toLocalIterator
    while(vertices.hasNext){
      
      val vertex = vertices.next()._2._2
      
      if(vertex.coord_x > maxX){
        maxX = vertex.coord_x
      }
      
      if(vertex.coord_y > maxY){
        maxY = vertex.coord_y
      }
      
      if(vertex.coord_x < minX){
        minX = vertex.coord_x
      }
      
      if(vertex.coord_y < minY){
        minY = vertex.coord_y
      }
      
    }
    
    val numCC = graph.vertices.first()._2._2.cc
   
    Shared.idCC_MaxXMaxY = Shared.idCC_MaxXMaxY += (numCC -> (maxX, maxY))
    Shared.idCC_MinXMinY = Shared.idCC_MinXMinY += (numCC -> (minX, minY))
  }
   
 /**
  *  This method computes the connected components final grid layout
  *  @param A map: each tuple describes the ID of the connected component and the number of its vertices
  */
  
 def computeComponentGridLayout(sortedMap : Map[Long, Long]) {
      
   var componentPadding = Shared.componentPadding
   var minRatioThreshold = Shared.minRatioThreshold
   
   var cursor : Point2D.Float = new Point2D.Float(0.0f, 0.0f);
   var tableOrigin : Point2D.Float = new Point2D.Float(0.0f, 0.0f);
   
	 val componentsNo = Shared.idCC_MaxXMaxY.size // idCC_MaxXMaxY.size == idCC_MinXMinY.size
	 
   var coloumnNo = (Math.ceil(Math.sqrt(componentsNo - 1)).toInt).toDouble 
   
   var componentSizeSorter = sortedMap.keySet.toArray
   var componentSizeSorterValues = sortedMap.values.toArray
   
   var maxID = componentSizeSorter(componentSizeSorter.length-1)
   var maxNo = componentSizeSorterValues(componentSizeSorter.length-1)
   
   var maxComponents = Shared.idCC_MaxXMaxY.get(maxID).get

   var translationCorrection = Shared.idCC_MinXMinY.get(maxID).get
	 
	 val tuple = Array(-translationCorrection._1, -translationCorrection._2, 1.0f, 
	     cursor.getX, cursor.getY)
	                                                                  // sul suo è cursor.x, cursor.y
	 Shared.offsets.put(maxID, tuple)
	 
	 cursor.setLocation((maxComponents._1 - translationCorrection._2) + componentPadding, 0.0f)
	 tableOrigin.setLocation(cursor);

   var coloumnMaxY = 0.0f;
   var counter = 1;  

   for(j <- 0 to componentSizeSorter.length-2){

			val currentComponent = componentSizeSorter(j)		
			maxComponents = Shared.idCC_MaxXMaxY.get(currentComponent).get
			var sizeRatio = (componentSizeSorterValues(j).toFloat/maxNo)
			translationCorrection =  Shared.idCC_MinXMinY.get(currentComponent).get

			if(sizeRatio < minRatioThreshold){
			  sizeRatio = minRatioThreshold;
			}
				
			
			var mc1 = maxComponents._1
			var mc2 = maxComponents._2
			
			mc1 = mc1 - translationCorrection._1
			mc2 = mc2 - translationCorrection._2
			mc1 = mc1 * sizeRatio;
			mc2 = mc2 * sizeRatio;
			
			maxComponents = (mc1, mc2)

			
			val tuple = Array(-translationCorrection._1, -translationCorrection._2, sizeRatio, 
	     cursor.getX, cursor.getY)                                                                

			Shared.offsets.put(currentComponent, tuple);

			if(maxComponents._2 > coloumnMaxY)
				coloumnMaxY = maxComponents._2.toFloat;

			if(counter % coloumnNo != 0){
				cursor.setLocation(cursor.getX + maxComponents._1 + componentPadding, cursor.getY);
				counter = counter+1;

			}else{

				cursor.setLocation(tableOrigin.getX, cursor.getY + coloumnMaxY + componentPadding);
				coloumnMaxY = 0.0f;
				counter = 1;
			}

		}
	 
 }
   
}