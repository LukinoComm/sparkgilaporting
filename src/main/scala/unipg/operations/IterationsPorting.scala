package unipg.operations

import org.apache.spark.graphx.Graph.graphToGraphOps
import org.apache.spark.graphx.VertexId
import unipg.utils.Counter
import unipg.utils.Temperature
import unipg.utils.VP

/**
 * This object contains the iterative method used to compute the convergence graph for each connected
 * components
 */

object IterationsPorting extends java.io.Serializable {
  
  import org.apache.spark.graphx.Graph
  import org.apache.spark.rdd.RDD
  import org.apache.spark.graphx.VertexId
  import org.apache.spark.graphx.EdgeDirection
  import org.apache.spark.graphx.Edge
  
  /**
   * This method defines a initial temperature and computes, until convergence, a pregel representation
   * for the input graph 
   * 
   * @param Maximum number of iterations for loop
   * @param Input Graph
   * @return The Graph after n-pregel execution
   */
  
  def computeGraphIteratively (maxNumIter : Int, ttl : Int, graph : Graph[(Int, VP),Boolean]) : (Graph[(Int, VP),Boolean]) = {
  
    var relationships = graph.edges
    
    var x : Double = 0.0
    var y : Double = 0.0

    val t = Temperature.setInitialTemperature(graph.vertices.count().toInt)
    
    val t_x_init = t._1
    val t_y_init = t._2
    
    var t_x = t_x_init
    var t_y = t_y_init
    
    val pp = new PregelPorting(ttl, t_x, t_y)
    
    //var vertices_previous_step = graph.vertices.collect()
    val num_vertices_threshold = (graph.vertices.count()*0.8).toInt

    var mingraph = graph.pregel(pp.initialMsg,Integer.MAX_VALUE,EdgeDirection.Out)(
        pp.vprog,pp.sendMsg,pp.mergeMsg).cache();
    
    //var new_vertices = mingraph.vertices.collect()

    var num_iter : Int = 0
    var check : Boolean = true
    
    while(check == true){   
      num_iter = num_iter+1
      
      if(Counter.c > num_vertices_threshold){
        check = false
        Counter.c = 0
        //println("Convergence")
      }
      
      if(num_iter==maxNumIter){
          //println("BREAK")
          Counter.c = 0
          check = false
      }
      
      else if(check==true){
        //println("Threshold not reached with count: "+Counter.c+"/"+num_vertices_threshold+" --> 80% vertices")
        print("N° Iter "+num_iter)

        // Reset info contained in the vertices of the graph
        var clean_vertex : RDD[(VertexId, (Int, VP))] = 
          mingraph.vertices.map{
          
           vertices => 

            vertices._2._2.map.clear()
            vertices._2._2.list_VertexID.clear()
            vertices._2._2.x_force = 0.0
            vertices._2._2.y_force = 0.0
            val ttl_reset= ttl
            (vertices._1,(ttl_reset, vertices._2._2))
           
         }.cache()
        

        t_x = Temperature.linearDecrease(t_x)
        t_y = Temperature.linearDecrease(t_y)
        
        pp.setTX(t_x)
        pp.setTY(t_y)
               
        mingraph = Graph(clean_vertex, relationships).cache();  
        Counter.c = 0
        
        mingraph = mingraph.pregel(pp.initialMsg, 
                                Integer.MAX_VALUE,
                                EdgeDirection.Out)(
                                pp.vprog,
                                pp.sendMsg,
                                pp.mergeMsg).cache();
        

      }
    }
    
    //println("Num_iter "+num_iter+"\n")
    return mingraph
  }
  
}