package unipg.operations

import unipg.utils.VP

/**
 * Class that holds base methods to reintegrate the vertices, inspired by GraphReintegration.java
 * (GiLA) 
 */

class GraphReintegration (k : Double, radius : Double) extends java.io.Serializable {
  
  import Array._
  import java.io.IOException
  
  def computeOneDegreeVerticesCoordinates(vertex : VP, shortestEdge : Int,
      quantity : Int, slopeAngle: Double, slopeStart : Double) : Array[Array[Double]] = {
    
    if(shortestEdge == Float.MaxValue){
      placeVerticesInGap(quantity, slopeAngle, slopeStart, (vertex.coord_x, vertex.coord_y));
      
		}else{
		  placeVerticesInGapWithShortestEdge(quantity, slopeAngle, slopeStart,  (vertex.coord_x, vertex.coord_y), shortestEdge);
		 }   
	}
  
  
  def placeVerticesInGap(quantity : Int, sector : Double, start : Double, myCoords : (Double, Double)) 
  : Array[Array[Double]] = {    
    vertexPlacer(quantity, sector, start, myCoords, radius * k);
  }
  
  
  def placeVerticesInGapWithShortestEdge(quantity : Int, sector : Double, start : Double, myCoords : (Double, Double),
      shortestEdge : Float) : Array[Array[Double]] = {    
    vertexPlacer(quantity, sector, start, myCoords, radius * shortestEdge);
  }

  
  
  def vertexPlacer(quantity : Long, sector : Double, start : Double, 
      myCoords : (Double, Double), effectiveRadius : Double) : Array[Array[Double]] = {
    
			if(quantity < 0){
			  val ret_empty = ofDim[Double](0,0)
			  return ret_empty
			}
 
			else {
			  
			  val result = ofDim[Double]((quantity.toInt),2)
  			val angularResolution : Double = sector/(quantity.toDouble);
  			val halfAngularResolution : Double = angularResolution/2;
  		  val quantityInt = quantity.toInt			
  			for(i<-0 to (quantityInt-1)){ 			  
  			  val currentDeg : Double = start + halfAngularResolution + i*angularResolution; 
  			  val x : Double = myCoords._1 + ((Math.cos(currentDeg)).toDouble).floatValue()*effectiveRadius;
  				val y : Double = myCoords._2 + ((Math.sin(currentDeg)).toDouble).floatValue()*effectiveRadius;  				
  				result(i.toInt)(0) = x;
  				result(i.toInt)(1) = y; 				
			  } 			
  		  return result; 
			}
  }
  
  def reconstructGraph(verticesToPlace : Array[Array[Double]], vertex : VP) = {
			
      val oneDegreesIt = vertex.adjacent_list_one_degree.toIterator
			for(i <- 0 to (verticesToPlace.length-1)){
				addSingleOneDegreeVertex(oneDegreesIt.next(), verticesToPlace(i), vertex);
			}			
			if(oneDegreesIt.hasNext)
				throw new IOException("One Edges iterator was not completely explored");		
		}
  
  def addSingleOneDegreeVertex(idOfOneEdge : Long, coordinatesOfVertexToPlace : Array[Double], neighborVertex : VP){
    GraphFaber.addVertexAndEdges(idOfOneEdge, coordinatesOfVertexToPlace, neighborVertex)
		}
  
  def reconstructGraphKeepingIterator(verticesToPlace : Array[Array[Double]], vertex : VP,
      woundIterator : Iterator[org.apache.spark.graphx.VertexId]) {   
    for(i<-0 to (verticesToPlace.length-1)){
				addSingleOneDegreeVertex(woundIterator.next(), verticesToPlace(i), vertex);
			}
		}
  
  
}