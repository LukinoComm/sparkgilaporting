package unipg.utils

/**
 * This class stores the attributes and properties of each vertex
 *
 */

class VP extends java.io.Serializable {
  
  var id = Long.MinValue
  
  var coord_x  = scala.util.Random.nextInt(101).toDouble;
  var coord_y  = scala.util.Random.nextInt(101).toDouble
 
  var x_force = 0.0;
  var y_force = 0.0;
  
  var list_VertexID = new scala.collection.mutable.ListBuffer[org.apache.spark.graphx.VertexId]();
  var adjacent_list_one_degree = new scala.collection.mutable.ListBuffer[org.apache.spark.graphx.VertexId]();
  
  var map = scala.collection.mutable.Map[org.apache.spark.graphx.VertexId,(Double, Double)](); 
  
  var cc = Long.MinValue

}