package unipg.utils

import scala.math

/**
 * This object deals with the calculation and updating of the temperature
 *
 */

object Temperature extends java.io.Serializable {
    
  /**
 	* Convenience method to update the temperature each time a new seeding phase is performed.
 	* 
 	*/
  
  def linearDecrease(initialTemperature : Double) : Double = {
    initialTemperature*0.95
  }
    
   /**
	 * The method is used to set the initial temperature, based on the number of vertices of the graph
	 * 
	 */
   
  def setInitialTemperature(noOfNodes : Int) : (Double,Double) = {
      
    val nl = Shared.nl;
		val nw = Shared.nw;
		val ns = Shared.ns;

		val k = ns + (math.sqrt(math.pow(nl, 2) + math.pow(nw, 2)))
		val tempConstant = 0.1
     
		
		val maxCurrent : Array[Double] = Array(100.0,100.0)
		val minCurrent : Array[Double] = Array(0.0,0.0)

		val w = maxCurrent(0) - minCurrent(0) + k
		val h = maxCurrent(1) - minCurrent(1) + k
		
		val ratio = h/w;

		val W = Math.sqrt(noOfNodes/ratio)*k;
		val H = ratio*W
		
		val correctedSizes = Array(W, H)
		val scaleFactors = Array(W/w, H/h)
		val temps = Array(W/tempConstant, H/tempConstant);
		
		return(temps(0),temps(1))
    }
    
    
    
  
  
  
}