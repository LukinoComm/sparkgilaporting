package unipg.utils

import org.apache.spark.graphx.Graph
import scala.Iterator
import org.apache.spark.graphx.EdgeDirection
import scala.collection.mutable.ListBuffer
import java.io.FileNotFoundException
import java.lang.NumberFormatException
import org.apache.spark.Aggregator
import org.apache.spark.graphx.VertexId
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.rdd.UnionRDD
import org.apache.spark.graphx.VertexRDD
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.SparkConf
import unipg.iO.InputGraph
import unipg.iO.ToGML
import unipg.operations.GraphFaber
import unipg.operations.PregelReintegration
import unipg.operations.VertexRemoval
import unipg.operations.IterationsPorting
import unipg.operations.MapForBoundingBox
import org.apache.spark.graphx.EdgeRDD
import unipg.operations.PregelReintegration

/**
 * This object contains the main method of the whole project.
 *
 */

object AlgorithmMain extends java.io.Serializable {
  
  /**
     * @param Time To Live
 		 * @param <Input-path> of the Graph
 		 * @param <Output-path>
   */
 
 def main(args: Array[String]) {
  
    if(args.size != 3){
      throw new IllegalArgumentException("Wrong number of parameters: please specify TTL, Input-Path, Output_path")
    }
    
    else {
    
      try{

        Logger.getLogger("org").setLevel(Level.OFF)
        Logger.getLogger("akka").setLevel(Level.OFF)
        
        //val conf = new SparkConf().setMaster("local")
        val conf = new SparkConf()
        conf.set("spark.driver.allowMultipleContexts", "true")
        /*
         * 
        conf.set("spark.driver.memory", "3g")
        conf.set("spark.executor.memory", "3g")
        conf.set("spark.executor.core", "3")
        * 
        */
        conf.setAppName("PortingPregel")
        val sc = SparkContext.getOrCreate(conf)  
   
        val timeToLive : Int = args(0).toInt 
        val path_File = args(1)
        
        var finalList : ListBuffer[Graph[(Int, VP),Boolean]] = new ListBuffer()
        
        // Create the graph
        val listGraphPartitionedByCC = InputGraph.getVerticesAndEdgesFromEdgesByPackageAndSC(sc, path_File, timeToLive)

        
        for(g <- listGraphPartitionedByCC){

          // Remonve OneDegreeVertices
          val ridotto = VertexRemoval.removeOneDegreeVertices(g).cache()
          
          // Compute Coordinates For Layout
          var mingraph = IterationsPorting.computeGraphIteratively(300, 3, ridotto).cache()
            
          // Add OneDegreeVertices
          val minEdgeAndRehabilitation = new PregelReintegration()
          val mingraphA = mingraph.pregel(minEdgeAndRehabilitation.initialMsg, 
                                  1,
                                  EdgeDirection.Out)(
                                  minEdgeAndRehabilitation.vprog,
                                  minEdgeAndRehabilitation.sendMsg,
                                  minEdgeAndRehabilitation.mergeMsg);    
          
          val finalGraph = Graph(mingraphA.vertices.++(sc.parallelize(GraphFaber.getVerticesList())), 
              mingraphA.edges.++(sc.parallelize(GraphFaber.getEdgesList())))
              
          finalList += finalGraph
            
        }   
        
        finalList.foreach{
          elem => MapForBoundingBox.mergeValue(elem)
        }
        
        val sortedMap = listGraphPartitionedByCC.map{
          graph =>
            (graph.vertices.first()._2._2.cc, graph.vertices.count())    
        }.toMap
   
        val mutableMap = collection.mutable.Map(sortedMap.toSeq: _*) 
        MapForBoundingBox.computeComponentGridLayout(mutableMap)
        
        var vertices = finalList(0).vertices 
        var edges = finalList(0).edges
        
        for(index <- 1 to (finalList.size-1)){
          vertices = (VertexRDD)(vertices ++ finalList(index).vertices)
          edges = EdgeRDD.fromEdges((edges ++ finalList(index).edges))
        }

        val result = Graph(vertices, edges)
                        
        if(listGraphPartitionedByCC.size != 1){
          result.vertices.foreach{
            vertex =>
            val cCValue = vertex._2._2.cc
            val ccOffset = Shared.offsets.get(cCValue).get 
              vertex._2._2.coord_x = ((vertex._2._2.coord_x + ccOffset(0))*ccOffset(2)) + ccOffset(3)
              vertex._2._2.coord_y = ((vertex._2._2.coord_y + ccOffset(1))*ccOffset(2)) + ccOffset(4)
          }
        }

        val nameGraph = path_File.split("/")
        val nameToOutput = nameGraph(nameGraph.length-1).split('.')
        val completePathOut = args(2)+nameToOutput(0)+".gml"
        
        ToGML.convertToGML(completePathOut, result)

        sc.cancelAllJobs()
        sc.clearJobGroup()
        sc.stop()


    }catch{
      case fnf: FileNotFoundException => println("File Not Found: Wrong or Illegal Path");
      case nfe: NumberFormatException => println("Error in Input Format");
    }
      
  }


 }
  
}