package unipg.utils

import scala.collection.mutable.Map
import org.apache.spark.graphx.VertexId

/**
 * This object stores some configurations and variables, that other scala-script use
 *
 */

object Shared extends java.io.Serializable {
  
  var offsets : Map[Long, Array[Double]]  = Map()
    
  var idCC_MaxXMaxY : Map[VertexId,(Double, Double)] = Map();
  var idCC_MinXMinY : Map[VertexId,(Double, Double)] = Map();
  
  // MapForBoundingBox
  var componentPadding = 20.0f
  var minRatioThreshold = 0.2f
  
  // Temperature
  val nl = 3;
  val nw = 3;
  val ns = 5;
  val C = 1;
  
  //Rehabilitation
  val radius = 0.2 
  val padding : Double = 20.0 * Math.PI / 180
  
  
}