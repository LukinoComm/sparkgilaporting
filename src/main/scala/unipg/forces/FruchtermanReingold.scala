package unipg.forces

import unipg.utils.Shared

/**
 * This object extends ComputeForce trait. 
 * Its methods provide for the calculation of forces according to the FruchtermanReingold paradigm
 */

object FruchtermanReingold extends ComputeForces {  
  
  val C = Shared.C;
  val nl = Shared.nl;
  val nw = Shared.nw;
  val ns = Shared.ns;
  
  
  /**
   * This method computes the repulsive force between two vertices
   * @param coordinates (x,y)
   * @return repulsive force (fx,fy)
   */
  
  def computeRepulsion(value: (Double, Double), message: (Double, Double)):(Double,Double) = {

		val k = ns + (math.sqrt(math.pow(nl, 2) + math.pow(nw, 2)))
		
		var delta_x = (value._1 - message._1)
    var delta_y = (value._2 - message._2) 

    var d = math.sqrt(math.pow(delta_x, 2) + math.pow(delta_y, 2))
    
    if(d < 0.001){
      d = 0.001
    }

    val x_disp = delta_x * k / math.pow(d, 2)
    val y_disp = delta_y * k / math.pow(d, 2)
 
    val kaQ = k*k * 0.05  
    (x_disp*kaQ,y_disp*kaQ)       
  }
  
  
  /**
   * This method computes the attractive force between two vertices
   * @param coordinates (x,y)
   * @return attractive force (fx,fy)
   */
  
  def computeAttraction(value: (Double, Double), message: (Double, Double)):(Double,Double) = {

      val k = ns + (math.sqrt(math.pow(nl, 2) + math.pow(nw, 2)))   
      
      var delta_x = (value._1 - message._1)
      var delta_y = (value._2 - message._2)
      
      var d = math.sqrt(math.pow(delta_x, 2) + math.pow(delta_y, 2))
      
      val x_disp = delta_x * d / k
      val y_disp = delta_y * d / k
      
      (x_disp*0.06,y_disp*0.06)          
  }
     
}