package unipg.forces

trait ComputeForces extends java.io.Serializable {
  
  /**
 	* Its trait is useful for defining the methods that the objects that extend it will define
 	*/

   def computeAttraction(value: (Double, Double), message: (Double, Double)) : (Double,Double)
   
   def computeRepulsion(value: (Double, Double), message: (Double, Double)) : (Double,Double)
  
}